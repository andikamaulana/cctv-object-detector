# USAGE
# python yolo_object_detection.py --input ../example_videos/janie.mp4 --output ../output_videos/yolo_janie.avi --yolo yolo-coco --display 0
# python yolo_object_detection.py --input ../example_videos/janie.mp4 --output ../output_videos/yolo_janie.avi --yolo yolo-coco --display 0 --use-gpu 1

# import the necessary packages
from imutils.video import FPS
import imutils
import numpy as np
import argparse
import cv2
import os
import face_recognition

##face detect

import pickle
import face_recognition
from sklearn import svm

encodings = []
names = []

test_dir = os.listdir('test_dir/')
# Training directory
train_dir = os.listdir('train_dir/')

# Loop through each person in the training directory
for person in train_dir:
    pix = os.listdir("train_dir/" + person)
    # Loop through each training image for the current person
    for person_img in pix:
        # Get the face encodings for the face in each image file
        face = face_recognition.load_image_file("train_dir/" + person + "/" + person_img)
        face_bounding_boxes = face_recognition.face_locations(face)

        #If training image contains exactly one face
        if len(face_bounding_boxes) == 1:
            face_enc = face_recognition.face_encodings(face)[0]
            # Add face encoding for current image with corresponding label (name) to the training data
            encodings.append(face_enc)
            names.append(person)
        else:
            print(person + "/" + person_img + " was skipped and can't be used for training")


clf = svm.SVC(gamma='scale')
clf.fit(encodings,names)

filename = 'model.pkl'
pickle.dump(clf, open(filename, 'wb'))

print(" loading face ....")
##end face detect

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", type=str, default="",
	help="path to (optional) input video file")
ap.add_argument("-o", "--output", type=str, default="",
	help="path to (optional) output video file")
ap.add_argument("-d", "--display", type=int, default=1,
	help="whether or not output frame should be displayed")
ap.add_argument("-y", "--yolo", required=True,
	help="base path to YOLO directory")
ap.add_argument("-c", "--confidence", type=float, default=0.5,
	help="minimum probability to filter weak detections")
ap.add_argument("-t", "--threshold", type=float, default=0.6,
	help="threshold when applyong non-maxima suppression")
ap.add_argument("-u", "--use-gpu", type=bool, default=0,
	help="boolean indicating if CUDA GPU should be used")
args = vars(ap.parse_args())

# load the COCO class labels our YOLO model was trained on
labelsPath = os.path.sep.join([args["yolo"], "coco.names"])
LABELS = open(labelsPath).read().strip().split("\n")


# initialize a list of colors to represent each possible class label
np.random.seed(42)
COLORS = np.random.randint(0, 255, size=(len(LABELS), 3),
	dtype="uint8")
# COLORS=[[255,0,0],[0,255,0]]

# derive the paths to the YOLO weights and model configuration
weightsPath = os.path.sep.join([args["yolo"], "yolov3.weights"])
configPath = os.path.sep.join([args["yolo"], "yolov3.cfg"])
# weightsPath = "sajam/yolov3.weights"
# configPath = "sajam/yolov3.cfg"
# load our YOLO object detector trained on COCO dataset (80 classes)
print("[INFO] loading YOLO from disk...")
net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)


# check if we are going to use GPU
if args["use_gpu"]:
	# set CUDA as the preferable backend and target
	print("[INFO] setting preferable backend and target to CUDA...")
	net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
	net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
# determine only the *output* layer names that we need from YOLO
ln = net.getLayerNames()
ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]

# #config 2

# labelsPath = "yolo-coco/coco.names"
# LABELS_p = open(labelsPath).read().strip().split("\n")
# # initialize a list of colors to represent each possible class label
# np.random.seed(42)
# COLORS_p = np.random.randint(0, 255, size=(len(LABELS_p), 3),
# 	dtype="uint8")

# net_p= cv2.dnn.readNetFromDarknet("yolo-coco/yolov3.cfg","yolo-coco/yolov3.weights")

# net_p.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
# net_p.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

# ln_p = net_p.getLayerNames()
# ln_p = [ln_p[i[0] - 1] for i in net_p.getUnconnectedOutLayers()]

# initialize the width and height of the frames in the video file
W = None
H = None

# initialize the video stream and pointer to output video file, then
# start the FPS timer
print("[INFO] accessing video stream...")
vs = cv2.VideoCapture(args["input"] if args["input"] else 0)

writer = None
fps = FPS().start()

# loop over frames from the video file stream
fpss=0
while True:
	# read the next frame from the file
	(grabbed, frame) = vs.read()

	# if the frame was not grabbed, then we have reached the end
	# of the stream
	if not grabbed:
		break
	# else:
	# 	frame = imutils.rotate_bound(frame, 270)
	# if the frame dimensions are empty, grab them
	if W is None or H is None:
		(H, W) = frame.shape[:2]

	#detect face
	face_locations = face_recognition.face_locations(frame)
	no = len(face_locations)
	for i in range(no):
		a,b,c,d = face_locations[i]
		test_image = frame[a:c, d:b]
		cv2.rectangle(frame, (d,a), (b,c), (0,255,0), 4)
		test_image_enc = face_recognition.face_encodings(test_image)[i]
		name = clf.predict([test_image_enc])#;print(name)
		font = cv2.FONT_HERSHEY_DUPLEX
		cv2.putText(frame, name[0], (d + 6, a - 6), font, 1.0, (255, 255, 255), 1)
	# construct a blob from the input frame and then perform a forward
	# pass of the YOLO object detector, giving us our bounding boxes
	# and associated probabilities
	blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416),
		swapRB=True, crop=False)

	net.setInput(blob)
	layerOutputs = net.forward(ln)

	# initialize our lists of detected bounding boxes, confidences,
	# and class IDs, respectively
	boxes = []
	confidences = []
	classIDs = []

	# loop over each of the layer outputs
	for output in layerOutputs:
		# loop over each of the detections
		for detection in output:
			# extract the class ID and confidence (i.e., probability)
			# of the current object detection
			scores = detection[5:]
			classID = np.argmax(scores)
			confidence = scores[classID]

			# filter out weak predictions by ensuring the detected
			# probability is greater than the minimum probability
			if confidence > args["confidence"]:
				# scale the bounding box coordinates back relative to
				# the size of the image, keeping in mind that YOLO
				# actually returns the center (x, y)-coordinates of
				# the bounding box followed by the boxes' width and
				# height
				box = detection[0:4] * np.array([W, H, W, H])
				(centerX, centerY, width, height) = box.astype("int")

				# use the center (x, y)-coordinates to derive the top
				# and and left corner of the bounding box
				x = int(centerX - (width / 2))
				y = int(centerY - (height / 2))

				# update our list of bounding box coordinates,
				# confidences, and class IDs
				boxes.append([x, y, int(width), int(height)])
				confidences.append(float(confidence))
				classIDs.append(classID)

	# apply non-maxima suppression to suppress weak, overlapping
	# bounding boxes
	idxs = cv2.dnn.NMSBoxes(boxes, confidences, args["confidence"],
		args["threshold"])

	# ensure at least one detection exists
	if len(idxs) > 0:
		# loop over the indexes we are keeping
		for i in idxs.flatten():

			# extract the bounding box coordinates
			(x, y) = (boxes[i][0], boxes[i][1])
			(w, h) = (boxes[i][2], boxes[i][3])

			# draw a bounding box rectangle and label on the frame
			color = [int(c) for c in COLORS[classIDs[i]]]
			if LABELS[classIDs[i]]=="Orang":
				cv2.rectangle(frame, (x, y), (x + w, y + h), color, 4)
				text = "{} ".format(LABELS[classIDs[i]],
					confidences[i])
				cv2.putText(frame, text, (x, y - 5),
					1, 2, color, 2)

# # ####config two
# 	blob_p = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416),
# 		swapRB=True, crop=False)
# 	net_p.setInput(blob_p)
# 	layerOutputs_p = net_p.forward(ln_p)
# 	# initialize our lists of detected bounding boxes, confidences,
# 	# and class IDs, respectively


# # 	# initialize our lists of detected bounding boxes, confidences,
# # 	# and class IDs, respectively
# 	boxes_p = []
# 	confidences_p = []
# 	classIDs_p = []

# # 	# loop over each of the layer outputs
# 	for output in layerOutputs_p:
# 		# loop over each of the detections
# 		for detection in output:
# 			# extract the class ID and confidence (i.e., probability)
# 			# of the current object detection
# 			scores = detection[5:]
# 			classID = np.argmax(scores)
# 			confidence = scores[classID]

# 			# filter out weak predictions by ensuring the detected
# 			# probability is greater than the minimum probability
# 			if confidence > 0.5:
# 				# scale the bounding box coordinates back relative to
# 				# the size of the image, keeping in mind that YOLO
# 				# actually returns the center (x, y)-coordinates of
# 				# the bounding box followed by the boxes' width and
# 				# height
# 				box = detection[0:4] * np.array([W, H, W, H])
# 				(centerX, centerY, width, height) = box.astype("int")

# 				# use the center (x, y)-coordinates to derive the top
# 				# and and left corner of the bounding box
# 				x = int(centerX - (width / 2))
# 				y = int(centerY - (height / 2))

# 				# update our list of bounding box coordinates,
# 				# confidences, and class IDs
# 				boxes_p.append([x, y, int(width), int(height)])
# 				confidences_p.append(float(confidence))
# 				classIDs_p.append(classID)

# 	# apply non-maxima suppression to suppress weak, overlapping
# 	# bounding boxes
#     # 

# 	idxss = cv2.dnn.NMSBoxes(boxes_p, confidences_p, 0.5, 0.5)

# 	# ensure at least one detection exists
# 	if len(idxss) > 0:
# 		# loop over the indexes we are keeping
# 		for i in idxss.flatten():
# 			# extract the bounding box coordinates
# 			(x, y) = (boxes_p[i][0], boxes_p[i][1])
# 			(w, h) = (boxes_p[i][2], boxes_p[i][3])

# 			# draw a bounding box rectangle and label on the frame
# 			color = [int(c) for c in COLORS_p[classIDs_p[i]]]
# 			cv2.rectangle(frame, (x, y), (x + w, y + h), color, 4)
# 			text = "{}  ".format(LABELS_p[classIDs_p[i]],confidences_p[i])
# 			cv2.putText(frame, text, (x, y - 5),1, 2, color, 2)

# # #end two

	# check to see if the output frame should be displayed to our
	# screen
	if args["display"] > 0:
		# # show the output frame
		# frame=cv2.resize(frame,(int(W/2),int(H/2)))
		# frame = imutils.rotate_bound(frame, 90)
		cv2.imshow("Frame", frame)

		key = cv2.waitKey(1) & 0xFF

		# if the `q` key was pressed, break from the loop
		if key == ord("q"):
			break

	# if an output video file path has been supplied and the video
	# writer has not been initialized, do so now
	if args["output"] != "" and writer is None:
		# initialize our video writer
		fourcc = cv2.VideoWriter_fourcc(*"mp4v")
		writer = cv2.VideoWriter(args["output"], fourcc, 20,
			(frame.shape[1], frame.shape[0]), True)

	# if the video writer is not None, write the frame to the output
	# video file
	if writer is not None:
		writer.write(frame)

	# update the FPS counter
	fps.update()

# stop the timer and display FPS information
fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))